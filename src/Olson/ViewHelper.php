<?php

namespace Olson;

/**
 * Description of ViewHelper
 *
 * @author maurizio
 */
class ViewHelper {

    static $services;

    static function getResourcesPath()
    {

        return htmlspecialchars(self::getProjectRoot() . 'app/resources', ENT_QUOTES, 'utf-8');
    }

    static function getProjectRoot()
    {
        self::setServices();
        $config = self::$services->get('config');
        return htmlspecialchars($config->getSettings('project', 'project_root'), ENT_QUOTES, 'utf-8');
    }

    private static function setServices()
    {
        self::$services = include __DIR__ . '../../services.php';
    }

}
