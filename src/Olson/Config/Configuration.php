<?php

namespace Olson\Config;

/**
 * Description of Config
 *
 * @author maurizio
 */
class Configuration {

    private $settingsArray = array();

    public function __construct()
    {
        $path = __DIR__. '/../../../app/settings/settings.php';
        include $path;
        $this->settingsArray = $global_settings;
    }

    public function getSettings($section = '', $item = '')
    {
        if ($item == '') {
            return $this->settingsArray[$section];
        } else {
            return $this->settingsArray[$section][$item];
        }
    }

}
