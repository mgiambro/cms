<?php
namespace Blog\Controller;

use Blog\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of MailController
 *
 * @author maurizio
 */
class MailController extends BaseController {

    function __construct()
    {
        parent::__construct();
        $this->config = $this->services->get('config');
    }

    public function indexAction(Request $request)
    {
        $data = array();
        $url = $request->get('url');

// if the url field is empty 
        if (isset($url) && $url == '') {
			
// then send the form to your email
            
            if (!$this->validateForm($request)){
                $data['validationMessage'] = 'Please press the back button and fill in all the fields';
                return $this->templating->render('pages/mailMessage.php', array('data' => $data));
            }
           
            $contactEmail = $this->config->getSettings('project', 'contact_email');
            $body = $this->prepareEmailBody($request);
            $headers = $this->prepareEmailHeaders($request);
            mail($contactEmail, 'Contact Form', $body, $headers, "-f <from " . $request->get('your_email') . ">");
        }
// otherwise, let the spammer think that they got their message through
        return $this->templating->render('pages/mailMessage.php', array('data' => $data));
    }

    private function validateForm(Request $request){
        $valid = true;
        $message = $request->get('your_enquiry');
        $name = $request->get('your_name');
        if($message == '' || $name == ''){
            $valid = false;
        }
        return $valid;
    }
    
    private function prepareEmailBody(Request $request)
    {

        $name = $request->get('your_name');
        $email = $request->get('your_email');
        $message = $request->get('your_enquiry');
            
        $body = "CONTACT MESSAGE FROM [sender]:     
Name:  $name
E-Mail: $email
Message: $message";
        return $body;
    }

   private function prepareEmailHeaders(Request $request)
    {
        // Use the submitters email if they supplied one     
        // (and it isn't trying to hack your form).     
        // Otherwise send from your email address.     
        $email = $request->get('your_email');
        $fromName = $request->get('your_name');
        if( $email &&!preg_match( "/[\r\n]/", $email)){
            $fromEmail = $email;
        } else {
            $fromEmail = "[sender]";
        }
        
        $headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
$headers .= 'From:  ' . $fromName . ' <' . $fromEmail .'>' . " \r\n" .
            'Reply-To: '.  $fromEmail . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            return $headers;
    }
    

}
