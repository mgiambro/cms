<?php

namespace Blog\Controller;

use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;

abstract class BaseController {

    protected $templating;
    protected $services;

    function __construct()
    {
        $loader = new FilesystemLoader(__DIR__ . '/../../../app/resources/views/%name%');

        $this->setServices();

        $this->templating = new PhpEngine(new TemplateNameParser(), $loader);
//        $twig = new Twig_Environment($loader, array(
 //        'cache' => '/../../../app/resources/cache',
//));
    }

    function setServices()
    {
        $this->services = include __DIR__ . '/../../../src/services.php';
    }



}
