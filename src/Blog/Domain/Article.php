<?php

namespace Blog\Domain;

/**
 * Description of Article
 * 
 * Class to handle articles
 * 
 * @author maurizio
 */
class Article {
    // Properties

    /**
     * @var int The article ID from the database
     */
    public $id = null;

    /**
     * @var int When the article was published
     */
    public $publicationDate = null;

    /**
     * @var string Full title of the article
     */
    public $title = null;

    /**
     * @var string A short summary of the article
     */
    public $summary = null;

    /**
     * @var string The HTML content of the article
     */
    public $content = null;

    /**
     * @var string The author of the article
     */
    public $user = null;

    private function __construct()
    {
        
    }

    public function toArray()
            {
        $articleAsArray = get_object_vars($this);
        
        return $articleAsArray;
    }
    
    public function fill($row = array())
    {
        if (isset($row['id'])) {
            $this->id = (int) $row['id'];
        }
        if (isset($row['publicationDate'])) {
            $this->publicationDate = $row['publicationDate'];
        }
        if (isset($row['title'])) {
            $this->title = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $row['title']);
        }
        if (isset($row['summary'])) {
            $this->summary = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $row['summary']);
        }
        if (isset($row['content'])) {
            $this->content = $row['content'];
        }
        if (isset($row['user'])) {
            $this->user = $row['user'];
        }
         // Parse and store the publication date
        if (isset($row['publicationDate'])) {
            $publicationDate = explode('-',  $row['publicationDate']);

            if (count($publicationDate) == 3) {
                list ( $y, $m, $d ) = $publicationDate;
                $this->publicationDate = mktime(0, 0, 0, $m, $d, $y);
            }
        }
    }

    /*
      public static function fromValues($id, $publicationDate, $title, $summary, $content, $user)
      {
      $instance = new self();
      $instance->id = $id;
      $instance->publicationDate = $publicationDate;
      $instance->title = $title;
      $instance->summary = $summary;
      $instance->content = $content;
      $instance->user = $user;
      return $instance;
      }
     */

    public static function create()
    {
          $instance = new self();
          return $instance;
    }
    
    public static function fromDbRow($row = array())
    {
        //  echo var_dump($row);
        $instance = new self();
        $instance->fill($row);
        return $instance;
    }

    function getId()
    {
        return $this->id;
    }

    function getPublicationDate()
    {
        return $this->publicationDate;
    }

    function getTitle()
    {
        return $this->title;
    }

    function getSummary()
    {
        return $this->summary;
    }

    function getContent()
    {
        return $this->content;
    }

    function getUser()
    {
        return $this->user;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setPublicationDate($publicationDate)
    {
        $this->publicationDate = $publicationDate;
    }

    function setTitle($title)
    {
        $this->title = $title;
    }

    function setSummary($summary)
    {
        $this->summary = $summary;
    }

    function setContent($content)
    {
        $this->content = $content;
    }

    function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Sets the object's properties using the values in the supplied array
     *
     * @param assoc The property values
     */
}
