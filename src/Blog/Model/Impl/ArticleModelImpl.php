<?php

namespace Blog\Model\Impl;

use Blog\Domain\Article;
use Blog\Model\ArticleModelInterface;
use Blog\Dao\DbDao;
use Katzgrau\KLogger\Logger;
use Olson\Config\Configuration;
use PDO;
use Blog\Model\BaseModel;

/**
 * Description of ArticleDaoPdoImpl
 *
 * @author maurizio
 */
class ArticleModelImpl extends BaseModel implements ArticleModelInterface {

    private $config;
    private $dao;
    private $dbTable = 'articles';
    private $logger;

    public function __construct(DbDao $dbDao, Configuration $config, Logger $logger = null)
    {
        $this->dao = $dbDao;
        $this->logger = $logger;
        $this->config = $config;
    }

    public function search($searchString, $order = "publicationDate DESC")
    {
        $articles = array();
        $sql = 'SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles WHERE (title LIKE "%' . $searchString . '%" OR summary LIKE "%' . $searchString . '%" OR content LIKE "%' . $searchString . '%" OR user = :searchString) ORDER BY ' . $this->mysql_real_escape_string($order) . ' LIMIT :numRows';

        $params = [['column' => 'searchString', 'value' => $searchString], ['column' => ':numRows', 'value' => 1000000, 'dataType' => PDO::PARAM_INT]];

        $qry = $this->dao->query($sql, $params, false);
        $rows = $qry->fetchAll();

        foreach ($rows as $row) {
            array_push($articles, Article::fromDbRow($row));
        }
        return $articles;
    }

    public function getList($numRows = 1000000, $order = "publicationDate DESC")
    {
        $articles = array();
        $sql = "SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles
            ORDER BY " . $this->mysql_real_escape_string($order) . " LIMIT :numRows";

        $params = [['column' => ':numRows', 'value' => (int) trim($numRows), 'dataType' => PDO::PARAM_INT]];

        $qry = $this->dao->query($sql, $params);
        $rows = $qry->fetchAll();

        foreach ($rows as $row) {
            array_push($articles, Article::fromDbRow($row));
        }
        return $articles;
    }

    public function delete($ref, $fieldref = 'ref')
    {
        $this->dao->dbDelete($this->dbTable, $ref, 'id');
    }

    public function getById($id)
    {
        $sql = 'select * from articles where id=:id';
        $params = [['column' => ':id', 'value' => $id]];
        $qry = $this->dao->query($sql, $params);
        $row = $qry->fetch();
        if ($row) {
            $article = Article::fromDbRow($row);
            //      echo var_dump($article);
            //    exit;
            return $article;
        }
    }

    public function insert(Array $article)
    {
        $this->dao->dbInsert($this->dbTable, $article);
    }

    public function convertFormToStorageArray($params)
    {
        $publicationDate = date("Y-m-d H:i:s", $this->parseDate($params->get('publicationDate')));
        $form = array(
            array('column' => 'id', 'value' => $params->get('articleId')),
            array('column' => 'title', 'value' => $this->cleanString($params->get('title'))),
            array('column' => 'summary', 'value' => $this->cleanString($params->get('summary'))),
            array('column' => 'content', 'value' => $params->get('content')),
            array('column' => 'publicationDate', 'value' => $publicationDate,));
        return $form;
    }

    public function update($ref, $fields, $fieldref = 'ref')
    {
        $this->dao->dbUpdate($this->dbTable, $ref, $fields, $fieldref);
    }

    function mysql_real_escape_string($unescapedString)
    {
        $returnString = str_replace
                (
                array("\x00", "\\", "\0", "\n", "\r", "'", '"', "\x1a")
                , ' ', $unescapedString
        );
        if ($returnString != $unescapedString) {
            $this->logger->alert('Escaped query string: ' . $returnString . ' does not match unescaped query string: ' . $unescapedString);
        }
        return $returnString;
    }

}
