<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Blog\Model;

/**
 * Description of BaseModel
 *
 * @author maurizio
 */
class BaseModel {

    protected function cleanString($inStr)
    {
        return preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $inStr);
    }

    protected function parseDate($date)
    {
        $returnDate = $date;
        
          // Parse and store the  date
        if (isset($date)) {
            $expDate = explode('-', $date);
            if (count($expDate) == 3) {
                list ( $y, $m, $d ) = $expDate;
                $returnDate = mktime(0, 0, 0, $m, $d, $y);
            }
        }

        return $returnDate;
    }

}
