<?php

namespace Blog\Dao;

/**
 * Description of DbDao
 *
 * @author maurizio
 */
interface DbDao {
    
    public function query($sql, $parms = array());
   
    public function dbUpdate($table, $ref, $fields, $fieldref = 'ref') ;
    
    public  function dbInsert($table, $fieldList);
    
    public function dbInsertUpdate($table, $fields, $refField='ref', $ref = '');
    
    public function dbDelete($table, $ref, $fieldref = 'ref');
    
    
}
