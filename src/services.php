<?php

// example.com/src/container.php
use Symfony\Component\DependencyInjection;
use Symfony\Component\DependencyInjection\Reference;

$sc = new DependencyInjection\ContainerBuilder();

/*
$sc->register('articleController', 'Blog\Controller\ArticleController')
        ->setArguments(array(new Reference('ArticleModel')));
*/
$sc->register('ArticleModel', 'Blog\Model\Impl\ArticleModelImpl')
        ->setArguments(array(new Reference('dbDao'), new Reference('config'), new Reference('logger')));

$sc->register('dbDao', 'Blog\Dao\Impl\DbPdoImpl')
        ->setArguments(array(new Reference('config'), new Reference('logger')));

$sc->register('config', 'Olson\Config\Configuration');

$sc->register('logger', 'Katzgrau\KLogger\Logger')
        ->setArguments(array(__DIR__ . '/../app/data/logs'));

return $sc;


