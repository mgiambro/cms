<?php include "include/header.php" ?>
 
      <h1>Article Archive</h1>
 
      <ul id="headlines" class="archive">
 <div id="main">
<?php foreach ( $data['articles'] as $article ) { ?>
 
        <li>
            <span style="font-weight: bold"><?php echo date('j F Y', $article->publicationDate)?></span><span style="padding-left:2em"><a href="../view-article/<?php echo $article->id?>"><?php echo htmlspecialchars( $article->title )?></a></span>
          <p class="summary"><?php echo htmlspecialchars( $article->summary )?></p>
        </li>
 
<?php } ?>
 
      </ul>
 
      <p><?php echo $data['totalRows']?> article<?php echo ( $data['totalRows'] != 1 ) ? 's' : '' ?> in total.</p>
 
      <p><a href="../home/">Return to Homepage</a></p>
</div>
<?php include "include/footer.php" ?>
