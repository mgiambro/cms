<?php include "include/header.php" ?>

<div class="rbroundbox">
    <div class="rbtop"><div></div></div>
    <div class="rbcontent">

        <div class="content">
            <h2>Latest Articles</h2>
            <p>Some blurb .. </p>
            <a href="../archive/" class="light">Article Archive</a>
        </div>
        <div id="panel-right"><div id="panel-right-inside">
                <?php foreach (array_slice($data['articles'],0,2) as $article) { ?>
                    <p>
                        <span><?php echo date('j F', $article->publicationDate) ?></span><br/><a href="../view-article/<?php echo $article->id ?>"<?php echo htmlspecialchars($article->title) ?></a>
                    </p>
                    <p class="summary"><?php echo htmlspecialchars($article->summary) ?></p>
                <?php } ?>
            </div>
        </div>	

    </div>
    <div class="rbbot"><div></div></div>
</div>

<div id="main">

    <ul>

        <?php foreach ($data['articles'] as $article) { ?>

            <li>
                <span style="font-weight: bold"><?php echo date('j F', $article->publicationDate) ?></span><a href="../view-article/<?php echo $article->id ?>"<?php echo htmlspecialchars($article->title) ?></a>
                <p class="summary"><?php echo htmlspecialchars($article->summary) ?></p>
            </li>

        <?php } ?>

    </ul>
</div>
</div>




<?php include "include/footer.php" ?>