<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php
use Olson\ViewHelper;
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
    <head>
        <title><?php echo htmlspecialchars($data['pageTitle']) ?></title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-language" content="cs" />
        <meta name="author" lang="cs" content="Maurizio Giambrone; http://mauriziogiambrone.com" />
        <meta name="copyright" lang="cs" content="Maurizio Giambrone; http://mauriziogiambrone.com" />
        <meta name="description" content="..." />
        <meta name="keywords" content="..." />
        <meta name="robots" content="all,follow" />
        <link rel="stylesheet" type="text/css" href="/assets/css/screen.css"  media="screen,projection" />
        <link rel="stylesheet" type="text/css" href="/assets/css/screen.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css" />

    </head>
    <body>
        <div id="layout">

      <div id="header">

        <h1 id="logo"><a href="#" title="#">yourweb<span>.com</span></a></h1>
        <hr class="noscreen" />
        <div id="navigation">
        <ul>
          <li><a href="<?php echo ViewHelper::getProjectRoot() . 'home/' ?>" title="home">HOME</a></li>
          <li><a href="#" title="#">ABOUT US</a></li>
          <li><a href="#" title="#">PRODUCTS</a></li>
          <li><a href="#" title="#">CONTACT</a></li>
        </ul>
        </div>
        <hr class="noscreen" />

      </div>
