<?php include __DIR__ . "../../include/header.php" ?>
<div id="main"> 
<div id="adminHeader">
    <h2>CMS Admin</h2>
    <p>You are logged in as <b><?php echo htmlspecialchars($data['session']->get('username')) ?></b>. <a href="../logout/">Log out</a></p>
</div>

<h1><?php echo $data['pageTitle'] ?></h1>

<form action="<?php
$id = $data['article']->id ? '/' . $data['article']->id : '';
echo \Olson\ViewHelper::getProjectRoot() . 'web/front.php/' . $data['formAction'] . $id
?>" method="post">
    <input type="hidden" name="articleId" value="<?php echo $data['article']->id ?>"/>

    <?php if (isset($data['errorMessage'])) { ?>
        <div class="errorMessage"><?php echo $data['errorMessage'] ?></div>
    <?php } ?>

    <ul>

        <li>
            <label for="title">Article Title</label>
            <input type="text" name="title" id="title" placeholder="Name of the article" required autofocus maxlength="255" value="<?php echo htmlspecialchars($data['article']->title) ?>" />
        </li>

        <li>
            <label for="summary">Article Summary</label>
            <textarea name="summary" id="summary" placeholder="Brief description of the article" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars($data['article']->summary) ?></textarea>
        </li>

        <li>
            <label for="content">Article Content</label>
            <textarea name="content" id="content" placeholder="The HTML content of the article" required maxlength="100000" style="height: 30em;"><?php echo htmlspecialchars($data['article']->content) ?></textarea>
        </li>

        <li>
            <label for="publicationDate">Publication Date</label>
            <input type="date" name="publicationDate" id="publicationDate" placeholder="YYYY-MM-DD" required maxlength="10" value="<?php echo $data['article']->publicationDate ? date("Y-m-d", $data['article']->publicationDate) : "" ?>" />
        </li>
        <br/>
        <div class="buttons">
            <input type="submit" name="saveChanges" value="Save Changes" /><br/>
            <input type="submit" formnovalidate name="cancel" value="Cancel" />
        </div>

    </ul>



</form>

<?php if ($data['article']->id) { ?>
    <p><a href="../delete-article/<?php echo $data['article']->id ?>" onclick="return confirm('Delete This Article?')">Delete This Article</a></p>
<?php } ?>
 
</div>
</div>
<?php include __DIR__ . "../../include/footer.php" ?>
