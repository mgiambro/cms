<?php include __DIR__ . "../../include/header.php" ?>

<div id="main">
    <div id="adminHeader">
        <h2>CMS Admin</h2>
        <p>You are logged in as <b><?php echo htmlspecialchars($data['session']->get('username')) ?></b>. <a href="../logout/"?>Log out</a></p>
    </div>

    <h1>All Articles</h1>

    <?php if (isset($data['errorMessage'])) { ?>
        <div class="errorMessage"><?php echo $data['errorMessage'] ?></div>
    <?php } ?>




    <?php if (isset($data['statusMessage'])) { ?>
        <div class="statusMessage"><?php echo $data['statusMessage'] ?></div>
    <?php } ?>

    <table>
        <tr>
            <th>Publication Date</th>
            <th>Article</th>
        </tr>

        <?php foreach ($data['articles'] as $article) { ?>

            <tr onclick="location = '<?php echo \Olson\ViewHelper::getProjectRoot() . 'web/front.php/edit-article/' . $article->id ?>'">
                <td><?php echo date('j M Y', $article->publicationDate) ?></td>
                <td>
                    <?php echo $article->title ?>
                </td>
            </tr>

        <?php } ?>

    </table>

    <p><?php echo $data['totalRows'] ?> article<?php echo ( $data['totalRows'] != 1 ) ? 's' : '' ?> in total.</p>
    <p>&nbsp</p>
    <p style='font-size: .9em;'><a href="<?php echo \Olson\ViewHelper::getProjectRoot() . 'web/front.php/new-article/' ?>">Add a New Article</a></p>

</div>
</div>
<?php include __DIR__ . "../../include/footer.php" ?>