<?php include __DIR__ . "../../include/header.php" ?>

<div id="main">
      <form action="../login/" method="post" style="width: 50%;">
        <input type="hidden" name="login" value="true" />
 
<?php if ( isset( $data['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $data['errorMessage'] ?></div>
<?php } ?>
 
        <ul>
 
          <li>
            <label for="username">Username</label>
            <input type="text" name="username" id="username" placeholder="Your admin username" required autofocus maxlength="20" />
          </li>
 
          <li>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" placeholder="Your admin password" required maxlength="20" />
          </li>
    
<br/>
             <div class="buttons">
            <input type="submit" name="login" value="Login" />
        </div>
            
       
        </ul>
    
       
 
      </form>
</div>
 
</div>
</div>
<?php include __DIR__ . "../../include/footer.php" ?>