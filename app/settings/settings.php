<?php

$global_settings = array(
    'project' => array(
        'homepage_num_articles' => 5,
        'admin_user_name' => 'admin',
        'admin_user_password' => 'password',
        'project_root' => 'http://olsoncms.app/'
    ), //project
    'database' => array(
        'dsn' => 'mysql:dbname=cms;host=127.0.0.1',
        'host' => '127.0.0.1',
        'port' => '33060',
        'dbname' => 'cms',
        'username' => 'homestead',
        'password' => 'secret',
    ), //database
        )//outer array
?>
