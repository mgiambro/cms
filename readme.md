**INSTALLATION:**

Goby CMS uses the Dependency Management tool Composer, so you'll need to have this installed to use it. If you don't currently have composer installed on your system, everything to get
up and running can be found at (https://getcomposer.org/"Composer").

Once this is set up, extract the Goby CMS archive, open up a command prompt, navigate to the project root and run the following command:

composer install

This will install all the third party dependencies.

**CREATE THE DATABASE**

To create the database, simply create your database and run articles.sql on it to add the tables. This script is for MySQL, but should work on most database servers.

**CONFIGURE**

Once you have the database you need to add the details to the configuration settings. These are found in app/settings/settings.php.

    $global_settings = array(
    'project' => array(
        'homepage_num_articles' => 5,
        'admin_user_name' => 'admin',
        'admin_user_password' => 'youradminpassword',
        'project_root' => 'http://localhost/projects/cms/'
    ), //project
    'database' => array(
        'dsn' => 'mysql:dbname=cms;host=127.0.0.1',
        'host' => '127.0.0.1',
        'port' => '3306',
        'dbname' => 'cms',
        'username' => 'root',
        'password' => 'yourdbpassword',
    ), //database
        )//outer array

The application should now be ready to deploy to your web server. Once deployed, assuming you keep the project name as 'cms'  access with a url similar to:

http://yourserver/cms/web/front.php/home/

**PAGES**

There is a basic implementation to add pages to the CMS. Simply add your pages to app/resources/views/pages and access them via the pages route, which takes a parameter of pageName. 
This is the name of your file. For example, to access index.php in the pages directory, your url would be something like:

[youruservername]/[yourcmsname]/web/front.php/pages/index
