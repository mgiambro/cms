<?php

namespace Test\Dao;

use Blog\Model\Impl\ArticleModelImpl;
use PDO;

/**
 * Description of ArticleModelTest
 *
 * @author maurizio
 */
class ArticleModelTest extends \PHPUnit_Framework_TestCase {

    // Mock the DbPdo for model tests.
    private $table = 'articles';

    public function testSearchReturnsEmptySet()
    {

        $searchString = 'wibble';

        $articles = Array();

        $mockDao = $this->getMockBuilder('Blog\Dao\Impl\DbPdoImpl')
                ->setConstructorArgs(array(new \Olson\Config\Configuration))
                ->setMethods(array('query'))
                ->getMock();

        $mockStatement = $this->getMockBuilder('\PDOStatement')->getMock();

        $mockDao->method('query')
                ->willReturn($mockStatement);
        $mockStatement->method('fetchAll')
                ->willReturn(array($articles));

        $sql = 'SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles WHERE (title LIKE "%' . $searchString . '%" OR summary LIKE "%' . $searchString . '%" OR content LIKE "%' . $searchString . '%" OR user = :searchString) ORDER BY publicationDate DESC LIMIT :numRows';
        $params = [['column' => 'searchString', 'value' => $searchString], ['column' => ':numRows', 'value' => 1000000, 'dataType' => PDO::PARAM_INT]];

        $mockDao->expects($this->once())
                ->method('query')
                ->with($this->equalTo($sql), $this->equalTo($params));

        $ArticleModelImpl = new ArticleModelImpl($mockDao, new \Olson\Config\Configuration);

        $ArticleModelImpl->search($searchString);
    }

    public function testSearchReturnsResults()
    {

        $searchString = 'test';

        $articles = Array
            (
            'id' => 3, 'publicationDate' => '2010-04-24', 'title' => 'test 3',
            'summary' => 'a summary',
            'content' => 'Boo',
            'user' => 'Maurizio',
        );

        $mockDao = $this->getMockBuilder('Blog\Dao\Impl\DbPdoImpl')
                ->setConstructorArgs(array(new \Olson\Config\Configuration))
                ->setMethods(array('query'))
                ->getMock();

        $mockStatement = $this->getMockBuilder('\PDOStatement')->getMock();

        $mockDao->method('query')
                ->willReturn($mockStatement);
        $mockStatement->method('fetchAll')
                ->willReturn(array($articles));
        
        $sql = 'SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles WHERE (title LIKE "%' . $searchString . '%" OR summary LIKE "%' . $searchString . '%" OR content LIKE "%' . $searchString . '%" OR user = :searchString) ORDER BY publicationDate DESC LIMIT :numRows';
        $params = [['column' => 'searchString', 'value' => $searchString], ['column' => ':numRows', 'value' => 1000000, 'dataType' => PDO::PARAM_INT]];

        $mockDao->expects($this->once())
                ->method('query')
                ->with($this->equalTo($sql), $this->equalTo($params));

        $ArticleModelImpl = new ArticleModelImpl($mockDao, new \Olson\Config\Configuration);

        $ArticleModelImpl->search($searchString);
    }

    public function testGetList()
    {
        $articles = Array
            (
            'id' => 3, 'publicationDate' => '2010-04-24', 'title' => 'test 3',
            'summary' => 'a summary',
            'content' => 'Boo',
            'user' => 'Maurizio',
        );

        $mockDao = $this->getMockBuilder('Blog\Dao\Impl\DbPdoImpl')
                ->setConstructorArgs(array(new \Olson\Config\Configuration))
                ->setMethods(array('query'))
                ->getMock();

        $mockStatement = $this->getMockBuilder('\PDOStatement')->getMock();

        $mockDao->method('query')
                ->willReturn($mockStatement);
        $mockStatement->method('fetchAll')
                ->willReturn(array($articles));

        $sql = "SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles
            ORDER BY publicationDate DESC LIMIT :numRows";

        $params = [['column' => ':numRows', 'value' => 1000000, 'dataType' => PDO::PARAM_INT]];

        $mockDao->expects($this->once())
                ->method('query')
                ->with($this->equalTo($sql), $this->equalTo($params));

        $ArticleModelImpl = new ArticleModelImpl($mockDao, new \Olson\Config\Configuration);

        $ArticleModelImpl->getList();
    }

    public function testDelete()
    {
        $mockDao = $this->getMockBuilder('Blog\Dao\Impl\DbPdoImpl')
                ->setConstructorArgs(array(new \Olson\Config\Configuration))
                ->setMethods(array('dbDelete'))
                ->getMock();

        $mockDao->expects($this->once())
                ->method('dbDelete')
                ->with($this->equalTo($this->table), $this->equalTo(1), $this->equalTo('id'));

        $ArticleModelImpl = new ArticleModelImpl($mockDao, new \Olson\Config\Configuration);

        $ArticleModelImpl->delete(1, 'id');
    }

    public function testUpdate()
    {
        $mockDao = $this->getMockBuilder('Blog\Dao\Impl\DbPdoImpl')
                ->setConstructorArgs(array(new \Olson\Config\Configuration))
                ->setMethods(array('dbUpdate'))
                ->getMock();

        $values = ['publicationDate' => date('2010-04-24 17:15:23'), 'title' => 'updated title', 'summary' => 'updated summary', 'content' => 'updated content', 'user' => 'Maurizio'];

        $mockDao->expects($this->once())
                ->method('dbUpdate')
                ->with($this->equalTo($this->table), $this->equalTo(1), $this->equalTo($values), $this->equalTo('id'));

        $ArticleModelImpl = new ArticleModelImpl($mockDao, new \Olson\Config\Configuration);

        $ArticleModelImpl->update(1, $values, 'id');
    }

    public function testGetById()
    {
        $mockDao = $this->getMockBuilder('Blog\Dao\Impl\DbPdoImpl')
                ->setConstructorArgs(array(new \Olson\Config\Configuration))
                ->setMethods(array('query'))
                ->getMock();

        $mockDao->method('query')
                ->willReturn(new \PDOStatement);

        $sql = 'select * from articles where id=:id';
        $params = [['column' => ':id', 'value' => 1]];

        $mockDao->expects($this->once())
                ->method('query')
                ->with($this->equalTo($sql), $this->equalTo($params));

        $ArticleModelImpl = new ArticleModelImpl($mockDao, new \Olson\Config\Configuration);

        $ArticleModelImpl->getById(1);
    }

    public function testInsert()
    {
        $mockDao = $this->getMockBuilder('Blog\Dao\Impl\DbPdoImpl')
                ->setConstructorArgs(array(new \Olson\Config\Configuration))
                ->setMethods(array('dbInsert'))
                ->getMock();

        $values = ['publicationDate' => date('2010-04-24 17:15:23'), 'title' => 'test 3', 'summary' => 'a summary', 'content' => 'Boo', 'user' => 'Maurizio'];

        $mockDao->expects($this->once())
                ->method('dbInsert')
                ->with($this->equalTo($this->table), $this->equalTo($values));

        $ArticleModelImpl = new ArticleModelImpl($mockDao, new \Olson\Config\Configuration);

        $ArticleModelImpl->insert($values);
    }

}
