<?php

namespace Test\Dao;

use Test\Dao\CmsDatabaseTest;
use Blog\Dao\Impl\DbPdoImpl;
use Olson\Config\Configuration;
use Katzgrau\KLogger\Logger;
use Blog\Domain\Article;
use \PDO;

/**
 * Description of DbPdoTest
 *
 * @author maurizio
 */
class DbPdoTest extends CmsDatabaseTest {

    protected function getDataSet()
    {
        return $this->createFlatXmlDataSet(__DIR__ . '/articleFlatXmlFixture.xml');
    }

    public function testQuery()
    {
        $config = new Configuration();
        $logger = new Logger(__DIR__ . '/../../app/data/logs');
        $this->assertEquals(2, $this->getConnection()->getRowCount('articles'), "Pre-Condition");
        $dbPdoImpl = new DbPdoImpl($config, $logger);
        $params = array(array('column' => 'id', 'value' => 2, 'dataType' => PDO::PARAM_INT));
        $sql = 'select * from articles where id=:id';

        $queryRes = $dbPdoImpl->query($sql, $params);

        $this->assertEquals(1, $queryRes->rowCount());
    }

    public function testDbDelete()
    {
        $config = new Configuration();
        $logger = new Logger(__DIR__ . '/../../app/data/logs');

        $this->assertEquals(2, $this->getConnection()->getRowCount('articles'), "Pre-Condition");

        $dbPdoImpl = new DbPdoImpl($config, $logger);
        $dbPdoImpl->dbDelete('articles', 2, 'id');

        $this->assertEquals(1, $this->getConnection()->getRowCount('articles'), "Inserting failed");
    }

    public function testDbInserOrUpdate_Update()
    {
        $config = new Configuration();
        $logger = new Logger(__DIR__ . '/../../app/data/logs');
        $this->assertEquals(2, $this->getConnection()->getRowCount('articles'), "Pre-Condition");
        $dbPdoImpl = new DbPdoImpl($config, $logger);
        $params = [['column' => 'id', 'value' => 2]];
        $sql = 'select * from articles where id=:id';

        $queryRow1 = $dbPdoImpl->query($sql, $params)->fetch();

        $this->assertNotNull($queryRow1);

        $newValues = array(array('column' => 'publicationDate', 'value' => date('2016-04-24')),
            array('column' => 'title', 'value' => 'Changed Title'),
            array('column' => 'summary', 'value' => 'Changed Summary'),
            array('column' => 'content', 'value' => 'Changed Content'),
            array('column' => 'user', 'value' => 'Salvador Dali'));

        $dbPdoImpl->dbInsertUpdate('articles', $newValues, 'id', $queryRow1['id']);

        $queryRow2 = $dbPdoImpl->query($sql, $params)->fetch();

        $updatedArticle = Article::fromDbRow($queryRow2);

        $this->assertEquals('Changed Title', $updatedArticle->getTitle());
        $this->assertEquals(1461456000, $updatedArticle->getPublicationDate());
        $this->assertEquals('Changed Summary', $updatedArticle->getSummary());
        $this->assertEquals('Changed Content', $updatedArticle->getContent());
        $this->assertEquals('Salvador Dali', $updatedArticle->getUser());
    }

    public function testDbInserOrUpdate_Insert()
    {
        $config = new Configuration();
        $logger = new Logger(__DIR__ . '/../../app/data/logs');
        $sql = 'select * from articles where id=:id';
        $params = [['column' => 'id', 'value' => 3]];

        $this->assertEquals(2, $this->getConnection()->getRowCount('articles'), "Pre-Condition");

        $dbPdoImpl = new DbPdoImpl($config, $logger);

        $values = array(array('column' => 'publicationDate', 'value' => date('2016-04-24')),
            array('column' => 'title', 'value' => 'test 3'),
            array('column' => 'summary', 'value' => 'a summary'),
            array('column' => 'content', 'value' => 'Boo'),
            array('column' => 'user', 'value' => 'Maurizio'));

        $dbPdoImpl->dbInsertUpdate('articles', $values, 'id');

        $queryRow2 = $dbPdoImpl->query($sql, $params)->fetch();

        $updatedArticle = Article::fromDbRow($queryRow2);

        $this->assertEquals('test 3', $updatedArticle->getTitle());
        $this->assertEquals(1461456000, $updatedArticle->getPublicationDate());
        $this->assertEquals('a summary', $updatedArticle->getSummary());
        $this->assertEquals('Boo', $updatedArticle->getContent());
        $this->assertEquals('Maurizio', $updatedArticle->getUser());

        $this->assertEquals(3, $this->getConnection()->getRowCount('articles'), "Inserting failed");
    }

    public function testDbUpdate()
    {
        $config = new Configuration();
        $logger = new Logger(__DIR__ . '/../../app/data/logs');
        $this->assertEquals(2, $this->getConnection()->getRowCount('articles'), "Pre-Condition");
        $dbPdoImpl = new DbPdoImpl($config, $logger);
        $params = [['column' => 'id', 'value' => 2]];
        $sql = 'select * from articles where id=:id';

        $queryRow1 = $dbPdoImpl->query($sql, $params)->fetch();

        $this->assertNotNull($queryRow1);

        $newValues = array(array('column' => 'publicationDate', 'value' => date('2016-04-24')),
            array('column' => 'title', 'value' => 'Changed Title'),
            array('column' => 'summary', 'value' => 'Changed Summary'),
            array('column' => 'content', 'value' => 'Changed Content'),
            array('column' => 'user', 'value' => 'Salvador Dali'));
        
        $dbPdoImpl->dbUpdate('articles', $queryRow1['id'], $newValues, 'id');

        $queryRow2 = $dbPdoImpl->query($sql, $params)->fetch();

        $updatedArticle = Article::fromDbRow($queryRow2);

        $this->assertEquals('Changed Title', $updatedArticle->getTitle());
        $this->assertEquals(1461456000, $updatedArticle->getPublicationDate());
        $this->assertEquals('Changed Summary', $updatedArticle->getSummary());
        $this->assertEquals('Changed Content', $updatedArticle->getContent());
        $this->assertEquals('Salvador Dali', $updatedArticle->getUser());
    }

    public function testDbInsert()
    {
        $config = new Configuration();
        $logger = new Logger(__DIR__ . '/../../app/data/logs');
        $sql = 'select * from articles where id=:id';
        $params = [['column' => 'id', 'value' => 3]];

        $this->assertEquals(2, $this->getConnection()->getRowCount('articles'), "Pre-Condition");

        $dbPdoImpl = new DbPdoImpl($config, $logger);

        $values = array(
            array('column' => 'publicationDate', 'value' => date('2016-04-24')),
            array('column' => 'title', 'value' => 'test 3'),
            array('column' => 'summary', 'value' => 'a summary'),
            array('column' => 'content', 'value' => 'Boo'),
            array('column' => 'user', 'value' => 'Maurizio'));


        $dbPdoImpl->dbInsert('articles', $values);

        $queryRow2 = $dbPdoImpl->query($sql, $params)->fetch();

        $updatedArticle = Article::fromDbRow($queryRow2);

        $this->assertEquals('test 3', $updatedArticle->getTitle());
        $this->assertEquals(1461456000, $updatedArticle->getPublicationDate());
        $this->assertEquals('a summary', $updatedArticle->getSummary());
        $this->assertEquals('Boo', $updatedArticle->getContent());
        $this->assertEquals('Maurizio', $updatedArticle->getUser());

        $this->assertEquals(3, $this->getConnection()->getRowCount('articles'), "Inserting failed");
    }

    public function testInjects()
    {
        $config = new Configuration();
        $logger = new Logger(__DIR__ . '/../../app/data/logs');
        $dboPdoImpl = new DbPdoImpl($config, $logger);
        //    $dboPdoImpl->logSomething();
        $this->assertNotNull($dboPdoImpl->db);
        $this->assertNotNull($dboPdoImpl->logger);
    }

}
